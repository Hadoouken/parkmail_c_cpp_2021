#include <gtest/gtest.h>
#include <fstream>
#include <sstream>

std::string readFileIntoString(const std::string &path) {
    std::ifstream input_file(path);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << path << "'" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::string((std::istreambuf_iterator<char>(input_file)),
                       std::istreambuf_iterator<char>());
}

extern "C" {
#include "products.h"
#include "utils.h"
}

TEST(LibTest, input_test_1) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test1.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_TRUE(success);
    EXPECT_EQ(products->products_num, 2);
    EXPECT_EQ(std::string(products->configs[0].name), "name1");
    EXPECT_EQ(std::string(products->configs[0].f_class), "class1");
    EXPECT_EQ(std::string(products->configs[1].name), "name2");
    EXPECT_EQ(products->configs[0].install_date->tm_sec, 0);
    EXPECT_EQ(products->configs[0].install_date->tm_min, 0);
    EXPECT_EQ(products->configs[0].install_date->tm_hour, 0);
    EXPECT_EQ(products->configs[0].install_date->tm_mday, 3);
    EXPECT_EQ(products->configs[0].install_date->tm_mon, 4);
    EXPECT_EQ(products->configs[0].install_date->tm_year, 113);
    EXPECT_EQ(products->configs[0].install_date->tm_isdst, 0);
    EXPECT_TRUE(products->configs[1].last_update_date == NULL);
    delete_products(products);
}

TEST(LibTest, input_test_2) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test2.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_FALSE(success);
    delete_products(products);
}

TEST(LibTest, input_test_3) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test3.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_FALSE(success);
    delete_products(products);
}

TEST(LibTest, input_test_4) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test4.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_FALSE(success);
    delete_products(products);
}

TEST(LibTest, input_test_5) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test5.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_FALSE(success);
    delete_products(products);
}

TEST(LibTest, output_test_1) {
    inst_products *products;
    FILE *fp;
    fp = fopen("../project/tests/test_data/test6.txt", "r");
    bool success = create(&products, fp);
    fclose(fp);
    EXPECT_TRUE(success);
    FILE *fp_output;
    fp_output = fopen("../project/tests/test_data/output.txt", "w");
    print_products_info(products, fp_output);
    fclose(fp_output);
    std::string to_test = readFileIntoString(
            "../project/tests/test_data/output.txt");
    std::string test = readFileIntoString(
            "../project/tests/test_data/test6_output.txt");
    EXPECT_EQ(to_test, test);
    delete_products(products);
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
