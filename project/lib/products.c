#include "products.h"
#include <stdlib.h>
#include <string.h>
#include "utils.h"

bool init_installed_products(size_t products_num, inst_products *products) {
    if (!products || !products_num) {
        return false;
    }
    products->products_num = products_num;
    products->configs = (conf *) malloc(products->products_num * sizeof(conf));
    if (!products->configs) {
        return false;
    }
    for (size_t i = 0; i < products->products_num; ++i) {
        products->configs[i].name = NULL;
        products->configs[i].f_class = NULL;
        products->configs[i].version = 0;
        products->configs[i].install_date = NULL;
        products->configs[i].last_update_date = NULL;
    }
    return true;
}

bool create(inst_products **new_products, FILE *stream) {
    if (!new_products) {
        return false;
    }
    *new_products = (inst_products *) malloc(
            sizeof(inst_products));
    if (!*new_products) {
        return false;
    }
    inst_products *products = *new_products;
    products->products_num = 0;
    products->configs = NULL;
    size_t num;
    printf("Введите количество установленных программных продуктов\n");
    if (!read_num(&num, stream)) {
        printf("Ошибка: недействительное количество установленных программ\n");
        return false;
    }
    if (!init_installed_products(num, products)) {
        return false;
    }
    for (size_t i = 0; i < products->products_num; ++i) {
        bool success = fill_in_config(&(products->configs[i]), stream);
        if (!success) {
            return false;
        }
    }
    return true;
}

bool fill_in_config(conf *config, FILE *stream) {
    if (!config) {
        return false;
    }
    printf("Введите имя продукта\n");
    if (!read_string(&config->name, stream)) {
        printf("Ошибка: Недействительное имя продукта\n");
        return false;
    }
    printf("Введите класс продукта\n");
    if (!read_string(&config->f_class, stream)) {
        printf("Ошибка: Недействительное название класса\n");
        return false;
    }
    printf("Введите версию продукта (целое число)\n");
    if (!read_num(&config->version, stream)) {
        printf("Ошибка: недействительная версия продукта\n");
        return false;
    }
    printf("Введите дату установки продукта в формате DD:MM:YYYY\n");
    if (!read_date(&config->install_date, stream) ||
        !is_valid_date(config->install_date)) {
        printf("Ошибка: недействительная дата\n");
        return false;
    }
    // Если первая версия продукта:
    // то обновления не было (last_update_date = NULL)
    // Если версия не первая
    // то создается и заполняется структура last_update_date
    if (config->version > 1) {
        printf("Введите дату последнего обновления в формате"
               "DD:MM:YYYY\n");
        if (!read_date(&config->last_update_date, stream) ||
            !is_valid_date(config->last_update_date)) {
            printf("Ошибка: недействительная дата\n");
            return false;
        }
        time_t install = mktime(config->install_date);
        time_t update = mktime(config->last_update_date);
        double seconds = difftime(update, install);
        if (seconds < 0) {
            printf("Ошибка: указана дата обновления,"
                   " которая раньше установки\n");
            return false;
        }
    }
    return true;
}

bool is_valid_date(struct tm *date) {
    if (!date) {
        return false;
    }
    time_t now = time(NULL);
    if (now == (time_t) (-1)) {
        return false;
    }
    double seconds_in_fifty_year = 50 * 365 * 24 * 60 * 60;
    double seconds = difftime(now, mktime(date));
    if (seconds < 0 || seconds > seconds_in_fifty_year) {
        return false;
    }
    return true;
}

bool copy_installed_products(inst_products *dest, const inst_products *src) {
    if (!dest || !src) {
        return false;
    }
    if (!init_installed_products(src->products_num, dest)) {
        return false;
    }
    for (size_t i = 0; i < dest->products_num; ++i) {
        dest->configs[i].name = (char *) malloc(
                (strlen(src->configs[i].name) + 1) * sizeof(char));
        if (!dest->configs[i].name) {
            return false;
        }
        int snprintf_ret;
        snprintf_ret = snprintf(dest->configs[i].name,
                                strlen(src->configs[i].name) + 1,
                                "%s", src->configs[i].name);
        if (snprintf_ret < 0) {
            return false;
        }

        dest->configs[i].f_class = (char *) malloc(
                (strlen(src->configs[i].f_class) + 1) * sizeof(char));
        if (!dest->configs[i].f_class) {
            return false;
        }
        snprintf_ret = snprintf(dest->configs[i].f_class,
                                strlen(src->configs[i].f_class) + 1,
                                "%s", src->configs[i].f_class);
        if (snprintf_ret < 0) {
            return false;
        }

        dest->configs[i].version = src->configs[i].version;

        dest->configs[i].install_date = (struct tm *) malloc(sizeof(struct tm));
        if (!dest->configs[i].install_date) {
            return false;
        }
        *dest->configs[i].install_date = *src->configs[i].install_date;

        if (src->configs[i].last_update_date) {
            dest->configs[i].last_update_date = (struct tm *) malloc(
                    sizeof(struct tm));
            if (!dest->configs[i].last_update_date) {
                return false;
            }
            *dest->configs[i].last_update_date =
                    *src->configs[i].last_update_date;
        } else {
            dest->configs[i].last_update_date = NULL;
        }
    }
    return true;
}

inst_products *group_by_class(const inst_products *products) {
    if (!products) {
        return NULL;
    }
    size_t i = 0;
    conf tmp;
    inst_products *return_struct = (inst_products *)
            malloc(sizeof(inst_products));
    if (!return_struct) {
        return NULL;
    }
    if (!copy_installed_products(return_struct, products)) {
        return NULL;
    }
    while (i < return_struct->products_num) {
        for (size_t j = i + 1; j < return_struct->products_num; ++j) {
            if (!strcmp(return_struct->configs[i].f_class,
                        return_struct->configs[j].f_class)) {
                ++i;
                if (i != j) {
                    tmp = return_struct->configs[i];
                    return_struct->configs[i] = return_struct->configs[j];
                    return_struct->configs[j] = tmp;
                }
                if (strcmp(return_struct->configs[i - 1].name,
                           return_struct->configs[i].name) > 0) {
                    tmp = return_struct->configs[i];
                    return_struct->configs[i] = return_struct->configs[i - 1];
                    return_struct->configs[i - 1] = tmp;
                }
            }
        }
        ++i;
    }
    return return_struct;
}

bool print_products_info(const inst_products *products, FILE *stream) {
    if (!products) {
        return false;
    }
    time_t now = time(NULL);
    if (now == (time_t) (-1)) {
        return false;
    }
    inst_products *products_to_print = group_by_class(products);

    double seconds_in_half_year = 365. / 2 * 24 * 60 * 60;
    fprintf(stream, "%12s%12s%10s%30s\n", "class", "name", "version",
            "install date");
    for (size_t i = 0; i < products_to_print->products_num; ++i) {
        // Если продукт ни разу не обновлялся
        if (products_to_print->configs[i].version == 1) {
            time_t inst_t = mktime(products_to_print->configs[i].install_date);
            double seconds_since_install = difftime(now, inst_t);
            if (seconds_since_install > seconds_in_half_year) {
                fprintf(stream, "%12s", products_to_print->configs[i].f_class);
                fprintf(stream, "%12s", products_to_print->configs[i].name);
                fprintf(stream, "%10zu", products_to_print->configs[i].version);
                time_t install_time = mktime(
                        products_to_print->configs[i].install_date);
                fprintf(stream, "%30s", ctime(&install_time));
                fprintf(stream, "\n");
            }
        }
    }
    delete_products(products_to_print);
    return true;
}

void delete_products(inst_products *products) {
    if (!products) {
        return;
    }
    for (int i = 0; i < products->products_num; ++i) {
        if (products->configs[i].name) {
            free(products->configs[i].name);
        }
        if (products->configs[i].f_class) {
            free(products->configs[i].f_class);
        }
        if (products->configs[i].install_date) {
            free(products->configs[i].install_date);
        }
        if (products->configs[i].last_update_date) {
            free(products->configs[i].last_update_date);
        }
    }
    if (products->configs) {
        free(products->configs);
    }
    free(products);
}
