#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <time.h>

typedef struct product_config {
    char *name;
    char *f_class;
    size_t version;
    struct tm *install_date;
    struct tm *last_update_date;
} conf;

typedef struct installed_products {
    size_t products_num;
    conf *configs;
} inst_products;

bool init_installed_products(size_t products_num, inst_products *products);

bool create(inst_products **new_products, FILE *stream);

bool fill_in_config(conf *config, FILE *stream);

bool is_valid_date(struct tm *date);

bool copy_installed_products(inst_products *dest, const inst_products *src);

inst_products *group_by_class(const inst_products *products);

bool print_products_info(const inst_products *products, FILE *stream);

void delete_products(inst_products *products);
