#include <stdlib.h>
#include <string.h>
#include "utils.h"

#define BUFF_SIZE 10
#define DATE_BUFF_SIZE 12

bool read_string(char **string_to_write, FILE *stream) {
    if (!string_to_write) {
        return false;
    }
    char buff[BUFF_SIZE];
    if (fgets(buff, BUFF_SIZE, stream) == NULL) {
        return false;
    }
    if (buff[strlen(buff) - 1] != '\n' || strlen(buff) == 1) {
        return false;
    }
    buff[strlen(buff) - 1] = '\0';
    *string_to_write = (char *) malloc((strlen(buff) + 1) * sizeof(char));
    if (!*string_to_write) {
        return false;
    }
    int snprintf_ret;
    snprintf_ret = snprintf(*string_to_write, strlen(buff) + 1, "%s", buff);
    if (snprintf_ret < 0) {
        return false;
    }
    return true;
}

bool read_num(size_t *num, FILE *stream) {
    if (!num) {
        return false;
    }
    char buff[BUFF_SIZE];
    if (fgets(buff, BUFF_SIZE, stream) == NULL) {
        return false;
    }
    if (buff[strlen(buff) - 1] != '\n' || strlen(buff) == 1) {
        return false;
    }
    *num = strtol(buff, NULL, 10);
    if (!*num) {
        return false;
    }
    return true;
}

bool init_tm(struct tm *date) {
    if (!date) {
        return false;
    }
    date->tm_sec = 0;
    date->tm_min = 0;
    date->tm_hour = 0;
    date->tm_wday = 0;
    date->tm_yday = 0;
    date->tm_isdst = 0;
    date->tm_year = 0;
    date->tm_mon = 0;
    date->tm_mday = 0;
    return true;
}

bool read_date(struct tm **date_to_write, FILE *stream) {
    if (!date_to_write) {
        return false;
    }
    *date_to_write = (struct tm *) malloc(sizeof(struct tm));
    if (!*date_to_write) {
        return false;
    }
    struct tm *date = *date_to_write;
    if (!init_tm(date)) {
        return false;
    }
    char buff[DATE_BUFF_SIZE];
    if (fgets(buff, DATE_BUFF_SIZE, stream) == NULL) {
        return false;
    }
    if (buff[strlen(buff) - 1] != '\n') {
        printf("Ошибка: недействительная дата\n");
        return false;
    }
    int day, month, year;
    if (!string_to_date(buff, &day, &month, &year)) {
        printf("Ошибка: невозможно преобразовать строку в дату\n");
        return false;
    }
    if (!is_date_correct(day, month, year)) {
        return false;
    }
    const int year_start_count = 1900;
    date->tm_year = year - year_start_count;
    date->tm_mon = month - 1;
    date->tm_mday = day;

    return true;
}

bool string_to_date(const char *str_date, int *day, int *month, int *year) {
    if (!str_date || !day || !month || !year) {
        return false;
    }
    char *end;
    *day = (int) strtol(str_date, &end, 10);
    if (end[0] != ':' || !*day) {
        return false;
    }
    *month = (int) strtol(end + 1, &end, 10);
    if (end[0] != ':' || !*month) {
        return false;
    }
    *year = (int) strtol(end + 1, &end, 10);
    if (!*year) {
        return false;
    }

    return true;
}

bool is_date_correct(int day, int month, int year) {
    if (!(((day >= 1 && day <= 31) &&
           (month == 1 || month == 3 || month == 5 || month == 7 ||
            month == 8 || month == 10 || month == 12))
          || ((day >= 1 && day <= 30) &&
              (month == 4 || month == 6 || month == 9 || month == 11))
          || ((day >= 1 && day <= 28) && (month == 2))
          || ((day == 29 && month == 2 &&
               (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)))))) {
        printf("Ошибка: недействительный день месяца\n");
        return false;
    }
    if (month < 1 || month > 12) {
        printf("Ошибка: недействительный месяц\n");
        return false;
    }
    if (year < -9999 || year > 9999) {
        printf("Ошибка: недействительный год\n");
        return false;
    }
    return true;
}
