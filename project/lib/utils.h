#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <time.h>


bool read_string(char **string_to_write, FILE *stream);

bool read_num(size_t *num, FILE *stream);

bool init_tm(struct tm *date);

bool read_date(struct tm **date, FILE *stream);

bool string_to_date(const char *str_date, int *day, int *month, int *year);

bool is_date_correct(int day, int month, int year);
