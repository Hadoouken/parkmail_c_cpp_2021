#include "products.h"
#include <stdbool.h>
#include <stdio.h>

int main() {
    inst_products *products;
    bool success = create(&products, stdin);
    if (success) {
        print_products_info(products, stdout);
    }
    delete_products(products);
    return 0;
}
